from setuptools import setup


NAME = 'test_plugin'
DESCRIPTION = 'A plugin to test Yodine\'s plugin loading capabilities.'
AUTHOR = 'Gustavo6046'
REQUIRES_PYTHON = '>=3.5.0'
VERSION = '0.1.1'
LICENSE = 'MIT'


# What packages are required for this plugin to work?
REQUIRED = [
    'yodine',

    # Yodine plugin dependencies:
    'yodine_data',

    # General dependencies:
    # 'numpy'
]

with open('README.md') as readme:
    long_description = readme.read()

setup(
    name=NAME,
    version=VERSION,
    description=DESCRIPTION,
    long_description=long_description,
    long_description_content_type='text/markdown',
    author=AUTHOR,
    python_requires=REQUIRES_PYTHON,
    packages=['test_plugin'],

    entry_points={
        'yodine.plugin': ['test_plugin = test_plugin:loaded'],
    },
    install_requires=REQUIRED,
    license=LICENSE,
    classifiers=[
        # Trove classifiers
        # Full list: https://pypi.python.org/pypi?%3Aaction=list_classifiers
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: PyPy'
    ],
    package_data={NAME: ['assets/*', 'assets/**/*']},
)
