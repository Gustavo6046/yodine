# ============== test_plugin ==============
#             by Gustavo6046
# licensed under MIT
#
#   A plugin to test Yodine's plugin loading capabilities.

# This is the main code of your plugin.
# Here you will add all of the logic that will
# be used by your plugin.

# The lines below import all of the classes you'll
# use from Yodine.
from yodine.core.entity import EntityTemplate, System
from yodine.core.extension import ModLoader
from yodine.core.vector import ComponentVector

# Other general imports below.
import os



# This helper function will access assets for you :)
def open_asset(asset_name: str) -> file:
    return open(os.path.join(os.path.split(__file__)[0], 'data', asset_name))


# This function will be called when the plugin is
# loaded.
def loaded(loader: ModLoader):
    # Defines a routine, which may be used by this or other
    # plugins.
    @loader.routine()
    def moo():
        # This can later be accessed via loader.routine (see
        # MooSystem).
        with open_asset('moo.txt') as moofile:
            print(moofile.read().strip())

    # Defines a system to be registered by
    # the loader.
    @loader.system_type
    class MyPluginSystem(System):
        # This is a list of components that the entity MUST
        # have, and that will be passed to tick, render, and
        # to event handlers (on_*).
        component_types = ['name']

        # This is a list of components that the entity MAY
        # have, and that will be passed to tick, render, and
        # to event handlers (on_*).
        # -----
        # If those components are not present, the default values
        # are passed instead.
        component_defaults = {'postfix': "I'm a dumb plugin, teehee!"}

        # This method of a system is called every tick, i.e., everytime
        # the game updates itself.
        # It is responsible for updating the entity.
        def tick(self, entity: Entity, *args, **kwargs) -> None:
            print("TICK!")

        # This method of a system is called every time the game is rendered.
        # It is responsible for rendering the entity.
        def render(self, entity: Entity, window: pyglet.window.Window, name, postfix, *args, **kwargs) -> None:
            print("I will print {}... already did! {}".format(name, postfix))
    
    @loader.system_type
    class MooSystem(System):
        # Components that must have certain values
        # in order for this system to operate on
        # a certain entity. Note that 'operate' includes
        # `tick`, `render` and `on_*` system methods.
        component_checks = {'moo': 'YES!'}

        def render(self, entity: Entity, *args, **kwargs) -> None:
            # Grabs a routine and calls it.
            loader.routines.moo()
            
            # This is our moo() routine defined earlier!
            # How magic is that? :)

    # Defines an entity template, a way to specify how certian
    # entities should be created.
    @loader.template
    class MyPluginEntityType(EntityTemplate):
        # The name of this template. Used when looking it up.
        name = 'Cow'

        # The group of this template. Used when looking a specific group of templates up.
        group = 'living.passive'

        # A list of default components.
        default_components = [
            ('name', 'a cow'),
            ('postfix', 'MOOO!'),
            ('moo', 'YES!')
        ]


